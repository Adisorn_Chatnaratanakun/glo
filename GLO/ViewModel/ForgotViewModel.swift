//
//  ForgotViewModel.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 10/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class ForgotViewModel: NSObject {
    let connection = Connection()
    
    func forgotPin(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.forgotPin, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    func forgotGeneralPassword(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.forgotGeneralPassword, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
}
