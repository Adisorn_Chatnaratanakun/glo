//
//  AuthenticationViewModel.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class AuthenticationViewModel : NSObject {
    let connection = Connection()
    
    func registerUser(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.registerURL, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    //MARK: LogIn
    func generalLogIn(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.generalLogIn, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    func agentLogIn(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.agentLogIn, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    func logInWithPin(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.logInPin, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    //MARK: - LogOut
    func logOut(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.logOut, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    //MARK: - Pin
    func setPin(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.setPin, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    func setNewPIN(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.setNewPin, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
    
    func setNewPassword(view: UIView?, parameter:[String:Any]?, completion: @escaping (Status, JSON?, Error?) -> Void) {
        
        connection.startConnection(view: view, serviceURI: Constant.setNewPassword, httpMethod: .post, parameters: parameter) { (status, json, error) in
            
            switch (status) {
            case .success :
                if let json = json {
                    completion(.success, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            case .failure :
                completion(.failure, nil, error)
                break
            case .error :
                if let json = json {
                    completion(.error, json, nil)
                }else{
                    completion(.unknown, nil, nil)
                }
                break
            default :
                completion(.unknown, nil, nil)
                break
            }
        }
    }
}
