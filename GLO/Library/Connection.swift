//
//  Connection.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

typealias CompletionBlock = (_ status:Status,_ json:JSON?,_ error:Error?) -> Void

enum Status {
    case success
    case failure
    case unknown
    case error
}

class Connection: NSObject {
    
    var prefs = UserDefaults.standard
    //    var isShowPopup = false
    
    // MARK: -
    
    class func checkInternetConnection() -> Bool? {
        return NetworkReachabilityManager()?.isReachable
    }
    
    func createSessionManager() -> SessionManager {
        
        let sessionManager = SessionManager.default
        sessionManager.session.configuration.timeoutIntervalForRequest = 60
//        sessionManager.delegate.sessionDidReceiveChallenge = { session, challenge in
//            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
//            var credential: URLCredential?
//
//            var checkAuthChallenge : URLSession.AuthChallengeDisposition = URLSession.AuthChallengeDisposition.useCredential
//            var checkCredential : URLCredential?
//            if (pinningValidator.handle(challenge, completionHandler: { (session, credential) in
//                print("\n\n\n\(session)\n\(String(describing: credential))\n\n\n")
//                checkAuthChallenge = session
//                checkCredential = credential
//            })) {
//                disposition = checkAuthChallenge
//                credential = checkCredential
//
//            }else{
//                credential = sessionManager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
//                if credential != nil {
//                    disposition = .cancelAuthenticationChallenge
//                }
//            }
//            return (disposition, credential)
//        }
                sessionManager.delegate.sessionDidReceiveChallenge = { session, challenge in
                    var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
                    var credential: URLCredential?
                    if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                        disposition = URLSession.AuthChallengeDisposition.useCredential
                        credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
                    } else {
                        if challenge.previousFailureCount > 0 {
                            disposition = .cancelAuthenticationChallenge
                        } else {
                            credential = sessionManager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                            if credential != nil {
                                disposition = .useCredential
                            }
                        }
                    }
                    return (disposition, credential)
                }
        
        return sessionManager
    }
    
    // MARK: -
    
    func startConnection(view:UIView?, urlString:String, httpMethod:HTTPMethod, parameters :[String:Any]?, completion: CompletionBlock?){
        
        self.doConnection(view: view, urlString: urlString, httpMethod: httpMethod, parameters: parameters, completion: completion)
        
    }
    
    func startConnection(view:UIView?, urlString:String, httpMethod:HTTPMethod, parameters :[String:Any]?, isShowPopup:Bool, completion: CompletionBlock?){
        
        self.doConnection(view: view, urlString: urlString, httpMethod: httpMethod, parameters: parameters, isShowPopup: isShowPopup, completion: completion)
        
    }
    
    func startConnection(view:UIView?, serviceURI:String, httpMethod:HTTPMethod, parameters :[String:Any]?, completion: CompletionBlock?){
        
        print("Current base URL: \(Constant.baseAPIURL)")
        self.doConnection(view: view, urlString: Constant.baseAPIURL + serviceURI, httpMethod: httpMethod, parameters: parameters, completion: completion)
        
    }
    
    func startConnection(view:UIView?, serviceURI:String, httpMethod:HTTPMethod, parameters :[String:Any]?, isShowPopup:Bool, completion: CompletionBlock?){
        
        self.doConnection(view: view, urlString: Constant.baseAPIURL + serviceURI, httpMethod: httpMethod, parameters: parameters, isShowPopup: isShowPopup, completion: completion)
        
    }
    
    private func doConnection(view:UIView?, urlString:String, httpMethod:HTTPMethod, parameters :[String:Any]?, isShowPopup:Bool = true, completion: CompletionBlock?){
        
        let loadingView : UIView = UIView(frame: CGRect.zero)
        let loading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        if let view = view {
            loadingView.frame = view.bounds
            view.addSubview(loadingView)
            let loadingOverlayView : UIView = UIView(frame: view.bounds)
            loadingOverlayView.backgroundColor = UIColor.darkText
            loadingOverlayView.alpha = 0.20
            loadingView.addSubview(loadingOverlayView)
            loadingView.sendSubviewToBack(loadingOverlayView)
            
            loadingView.addSubview(loading)
            loadingView.bringSubviewToFront(loading)
            loading.color = UIColor.white//UIColor.darkGray//Theme.ThemeColor.colorOrange
            loading.center = view.center
            loading.startAnimating()
        }
        
        let header = self.getHeader()
        //        print("\n\n\n\(urlString)\(header)\n\n\n")
        //        weak var weakSelf = self
        //        let configuration = URLSessionConfiguration.default
        //        let timeout : TimeInterval = 60
        //        configuration.timeoutIntervalForRequest = timeout
        //        configuration.timeoutIntervalForResource = timeout
        //        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        
        //let sessionManager = self.createSessionManager()
        let sessionManager = Alamofire.SessionManager.default//self.createSessionManager()
        //weak var weakSelf = self
        var params = parameters
        if parameters == nil {
            params = ["UnuseParam":""]
        }
        sessionManager.request(urlString, method: httpMethod, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if loadingView.superview != nil {
                loading.stopAnimating()
                loadingView.removeFromSuperview()
            }
            if let htmlStatusCode : Int = response.response?.statusCode {
                if htmlStatusCode == 200 {
                    switch response.result {
                    case .success:
                        if let jsonResponse = response.result.value {
                            let json = JSON(jsonResponse)
                            //                            switch (json["responseStatus"]["code"].stringValue) {
                            //                            case "200" :
                            //                                completion?(.success, json, nil)
                            //                                break
                            //                            case "100" :
                            //                                //Singleton.shared.forceLogout(responseStatus: json)
                            //                                break
                            //                            default:
                            //                                if isShowPopup == true {
                            //                                    //Singleton.shared.displayAlert(responseStatus: json["responseStatus"])
                            //                                }
                            //                                completion?(.failure, json, nil)
                            //                                break
                            //                            }
                            completion?(.success, json, nil)
                            
                        }else {
                            completion?(.unknown, nil, nil)
                        }
                        break
                    case .failure(let error):
                        if isShowPopup == true {
                            //Singleton.shared.displayConnectionErrorAlert()
                        }
                        completion?(.failure, nil, error)
                        break
                    }
                }else{
                    if let jsonResponse = response.result.value {
                        let json = JSON(jsonResponse)
                        if isShowPopup == true {
                            //Singleton.shared.displayAlert(responseStatus: json["responseStatus"])
                        }
                        completion?(.failure, json, nil)
                    }else{
                        if isShowPopup == true {
                            //Singleton.shared.displayConnectionErrorAlert()
                        }
                        completion?(.failure, nil, nil)
                    }
                }
            }else{
                if isShowPopup == true {
                    //Singleton.shared.displayConnectionErrorAlert()
                }
                completion?(.failure, nil, nil)
            }
        }
    }
    
    func getHeader() -> HTTPHeaders {
        var requestId = UUID().uuidString
        //let customerid : String = Singleton.shared.getUsername()
        var headers : HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            "appId" : Constant.appId,
            "requestId" : requestId,
            "authToken": Singleton.shared.getAuthToken()
        ]
        
        //        if Singleton.shared.isEncodeParameter == true {
        //            headers = [
        //                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
        //                , "customercode" : Singleton.shared.encryptHeaderParameter(string: Singleton.shared.customerCode)
        //                , "sessionid" : Singleton.shared.encryptHeaderParameter(string: Singleton.shared.accessToken)
        //                , "customerid" : Singleton.shared.encryptHeaderParameter(string: customerid)
        //                , "verifytype" : Singleton.shared.encryptHeaderParameter(string: Singleton.shared.getVerifyType())
        //                , "usertype" : Singleton.shared.encryptHeaderParameter(string: Singleton.shared.userType)
        //                , "channel" : Singleton.shared.encryptHeaderParameter(string: Singleton.shared.iFisChannel)
        //                , "version" : AppInfo.APP_VERSION
        //            ]
        //        }else{
        //            headers = [
        //                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
        //                , "customercode" : Singleton.shared.customerCode
        //                , "sessionid" : Singleton.shared.accessToken
        //                , "customerid" : customerid
        //                , "verifytype" : Singleton.shared.getVerifyType()
        //                , "usertype" : Singleton.shared.userType
        //                , "channel" : Singleton.shared.iFisChannel
        //            ]
        //        }
        
        return headers
    }
    
}

