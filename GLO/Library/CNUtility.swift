//
//  CNUtility.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift


enum FormType : String {
    case typeOne = "typeOne"
    case typeTwo = "typeTwo"
    case typeThree = "typeThree"
    case typeButton = "typeButton"
}

class CNUtility: NSObject {
    
    class func dataWithHexString(hex: String) -> NSData {
        var hex = hex
        let data = NSMutableData()
        while (Array(hex).count > 0) {
            let c : String = String(hex[...hex.index(hex.startIndex, offsetBy:1)])
            hex = String(hex[hex.index(hex.startIndex, offsetBy:2)...])
            var ch: UInt32 = 0
            Scanner(string: c).scanHexInt32(&ch)
            data.append(&ch, length: 1)
        }
        return data
    }
    
    class func generateShar256(_ keyStr : String) -> String {
        let hash = keyStr.sha256()
        let index = hash.index(hash.startIndex, offsetBy: 16)
        
        return String(hash[..<index])//hash.substring(to: index)
    }
    
    class func aesDecryptionWithCBC(text : String , key : String, iv: String, padding: Padding = Padding.pkcs7) -> String {
        do {
            let aes = try AES(key: key, iv: iv, padding: padding)
            let nsdata : NSData = self.dataWithHexString(hex: text)
            let data : Data = Data.init(referencing: nsdata)
            let byteArray = [UInt8](data)
            let ciphertexts = try aes.decrypt(byteArray)
            if let str = String(data: Data(bytes: ciphertexts), encoding: String.Encoding.utf8) {
                return str
            }
        } catch {
            print(error)
            return "Error"
        }
        
        return ""
    }
    
    class func aesEncryptionWithCBC(text : String , key : String, iv: String, padding: Padding = Padding.pkcs7) -> String {
        do {
            
            let aes = try AES(key: key, iv: iv, padding: padding)
            var encryptor = try aes.makeEncryptor()
            
            var ciphertext = Array<UInt8>()
            ciphertext = try encryptor.update(withBytes: text.utf8.map({$0}))
            ciphertext += try encryptor.finish()
            
//            return text
            return ciphertext.toHexString()
        } catch {
            print(error)
            return "Error"
        }
    }
    
    class func aesDecryptionWithECB(text : String , key : String, padding: Padding = Padding.pkcs7) -> String {
        do {
            let aes = try AES(key: key.bytes, blockMode: ECB(), padding: padding)
            let nsdata : NSData = self.dataWithHexString(hex: text)
            let data : Data = Data.init(referencing: nsdata)
            let byteArray = [UInt8](data)
            let ciphertexts = try aes.decrypt(byteArray)
            if let str = String(data: Data(bytes: ciphertexts), encoding: String.Encoding.utf8) {
                return str
            }
        } catch {
            print(error)
            return "Error"
        }
        
        return ""
    }
    
    class func aesEncryptionWithECB(text : String , key : String, padding: Padding = Padding.pkcs7) -> String {
        do {
            
            let aes = try AES(key: key.bytes, blockMode: ECB(), padding: padding)
            var encryptor = try aes.makeEncryptor()
            
            var ciphertext = Array<UInt8>()
            ciphertext = try encryptor.update(withBytes: text.utf8.map({$0}))
            ciphertext += try encryptor.finish()
            
            //            return text
            return ciphertext.toHexString()
        } catch {
            print(error)
            return "Error"
        }
    }
    
    static func hexToStr(text: String) -> String {
        
        let regex = try! NSRegularExpression(pattern: "(0x)?([0-9A-Fa-f]{2})", options: .caseInsensitive)
        let textNS = text as NSString
        let matchesArray = regex.matches(in: textNS as String, options: [], range: NSMakeRange(0, textNS.length))
        let characters = matchesArray.map {
            Character(UnicodeScalar(UInt32(textNS.substring(with: $0.range(at: 2)), radix: 16)!)!)
        }
        
        return String(characters)
    }
    
    class func generateR1() -> String{
        
        let uuid = UUID().uuidString.replacingOccurrences(of: "-", with: "")
        
        let r1 = self.generateShar256(uuid.uppercased())
        print(r1)
        return r1
    }


}
