//
//  Singleton.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 9/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class UserAccount {
    let prefs = UserDefaults.standard
    
    func username() -> String {
        return self.prefs.string(forKey: "username") ?? ""
    }
    
    func password() -> String {
        return self.prefs.string(forKey: "password") ?? ""
    }
}

final class Singleton {
    
    private init() { }
    
    static let shared = Singleton()
    
    let deviceType = "ios"
    let prefs = UserDefaults.standard
    var currentAPI = "dev"
    
    //MARK: - UserDefault
    func saveAuthToken(token: String) {
        self.prefs.setValue(token, forKey: "authToken")
        self.prefs.synchronize()
    }
    
    func getAuthToken() -> String {
        return self.prefs.string(forKey: "authToken") ?? ""
    }
    
    func saveGloID(gloId: String) {
        self.prefs.setValue(gloId, forKey: "gloId")
        self.prefs.synchronize()
    }
    
    func getGloID() -> String {
        return self.prefs.string(forKey: "gloId") ?? ""
    }
    
    func saveUserAccount(username:String, password: String){
        self.prefs.setValue(username, forKey: "username")
        self.prefs.setValue(password, forKey: "password")
        self.prefs.synchronize()
    }
    
    func getUserAccount() -> UserAccount{
        return UserAccount()
    }
    
    func saveDeviceId(deviceId: String) {
        self.prefs.setValue(deviceId, forKey: "deviceId")
        self.prefs.synchronize()
    }
    
    func getDeviceId() -> String {
        return self.prefs.string(forKey: "deviceId") ?? ""
    }
}
