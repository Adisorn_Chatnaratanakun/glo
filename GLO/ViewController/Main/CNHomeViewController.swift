//
//  CNHomeViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import FBSDKLoginKit
class CNHomeViewController: UIViewController {
    
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var changePinButton: UIButton!
    @IBOutlet weak var updateProfileButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var name: UILabel!
    var logInType: String = ""
    var fbname : String = ""
    var fromFB : Bool = false
    let authenticationVM = AuthenticationViewModel()
    
    class func newInstance() -> UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNHomeViewController")
        
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    
    func setupUI() {
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
        
        self.updateProfileButton.addTarget(self, action: #selector(self.navigateToUpdateProfile), for: .touchUpInside)
        self.changePinButton.addTarget(self, action: #selector(self.navigateToChangePin), for: .touchUpInside)
        self.changePasswordButton.addTarget(self, action: #selector(self.navigateToChangePassword), for: .touchUpInside)
        self.logoutButton.addTarget(self, action: #selector(self.logOutDidTap), for: .touchUpInside)
        
        if fromFB {
            name.text = fbname
//            self.changePinButton.isHidden = true
//            self.updateProfileButton.isHidden = true
//            self.changePasswordButton.isHidden = true
        }else{
            self.name.text = self.logInType
        }
        
    }
    
    //MARK: Button Action
    @objc func logOutDidTap(){
        self.logOut(view: nil)
    }
    
    //MARK: Load Service
    func logOut(view: UIView?){
        if fromFB {
            if FBSDKAccessToken.currentAccessTokenIsActive() {
                FBSDKLoginManager().logOut()
            }
        }
        self.authenticationVM.logOut(view: view, parameter: nil) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Log Out: \(json)")
                    self.navigateToAuthenticationViewController()
                }
                break
            case .failure:
                break
            case .error:
                break
            case .unknown:
                break
            }
        }
    }
    
    // MARK: - Navigation
    
    @objc func navigateToChangePin(){
        if let vc = CNChangePinViewController.newInstance() as? CNChangePinViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func navigateToChangePassword(){
        if let vc = CNChangePasswordViewController.newInstance() as? CNChangePasswordViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func navigateToUpdateProfile(){
        if let vc = CNUpdateProfileViewController.newInstance() as? CNUpdateProfileViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func navigateToAuthenticationViewController(){
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let root = UIStoryboard(name: "Authentication", bundle: Bundle.main).instantiateInitialViewController()
            let newWindow = UIWindow()
            appDelegate.replaceWindow(newWindow)
            newWindow.rootViewController = root
        }
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
