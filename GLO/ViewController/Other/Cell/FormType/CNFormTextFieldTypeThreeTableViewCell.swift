//
//  CNFormTextFieldTypeThreeTableViewCell.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class CNFormTextFieldTypeThreeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var borderView: UITextField!
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
