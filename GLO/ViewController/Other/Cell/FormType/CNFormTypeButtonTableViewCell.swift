//
//  CNFormTypeButtonTableViewCell.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 7/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class CNFormTypeButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
