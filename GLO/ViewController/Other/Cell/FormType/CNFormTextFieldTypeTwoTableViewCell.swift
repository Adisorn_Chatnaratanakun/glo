//
//  CNFormTextFieldTypeTwoTableViewCell.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 7/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class CNFormTextFieldTypeTwoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var firstCheckboxView: UIView!
    @IBOutlet weak var firstCheckboxIconLabel: UILabel!
    @IBOutlet weak var firstCheckboxLabel: UILabel!
    @IBOutlet weak var firstCheckboxButton: CustomButton!
    
    @IBOutlet weak var secondCheckboxView: UIView!
    @IBOutlet weak var secondCheckboxIconLabel: UILabel!
    @IBOutlet weak var secondCheckboxLabel: UILabel!
    @IBOutlet weak var secondCheckboxButton: CustomButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
