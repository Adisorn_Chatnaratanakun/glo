//
//  CNRegisterViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 7/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import SwiftyJSON

//MARK: - CNRegister Format Type JSON
enum CNRegisterFormatType: String {
    
    case General = "General"
    case Agent = "Agent"
    
    func formatData() -> JSON {
        var json: JSON = JSON.null
        switch self {
        case .General:
            json = [
                [
                    "title": "Card ID"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "numpadType"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "cardNo"
                ],
                [
                    "title": "Gender"
                    ,"values": [
                        "Male",
                        "Female"
                    ]
                    ,"isMale": true
                    ,"isEncrypt": false
                    ,"cellType": FormType.typeTwo.rawValue
                    ,"paramKey": "gender"
                ],
                [
                    "title": "Name"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "name"
                ],
                [
                    "title": "Surname"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "surname"
                ],
                [
                    "title": "Birthdate (Ex. 1993-03-31)"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "birthdate"
                ],
                [
                    "title": "Password"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": true
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "password"
                ],
                [
                    "title": "Email"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "email"
                ],
                [
                    "title": "Mobile"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "mobile"
                ],
                [
                    "title": "Register"
                    ,"cellType": FormType.typeButton.rawValue
                    ,"paramKey": "register"
                ]
            ]
        case .Agent:
            json = [
                [
                    "title": "Card ID"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "numpadType"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "cardNo"
                ],
                [
                    "title": "Gender"
                    ,"values": [
                        "Male",
                        "Female"
                    ]
                    ,"isMale": true
                    ,"isEncrypt": false
                    ,"cellType": FormType.typeTwo.rawValue
                    ,"paramKey": "gender"
                ],
                [
                    "title": "Name"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "name"
                ],
                [
                    "title": "Surname"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "surname"
                ],
                [
                    "title": "Birthdate (Ex. 1993-03-31)"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "birthdate"
                ],
                [
                    "title": "Password"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": true
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "password"
                ],
                [
                    "title": "Email"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "email"
                ],
                [
                    "title": "Address"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "address"
                ],
                [
                    "title": "Store Address"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "store_address"
                ],
                [
                    "title": "Lottery Count"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "lottery_count"
                ],
                [
                    "title": "Mobile"
                    ,"value": ""
                    ,"isTextFieldEnable": true
                    ,"isSecureText": false
                    ,"isEncrypt": true
                    ,"keyboardType": "default"
                    ,"cellType": FormType.typeOne.rawValue
                    ,"paramKey": "mobile"
                ],
                [
                    "title": "Register"
                    ,"cellType": FormType.typeButton.rawValue
                    ,"paramKey": "register"
                ]
            ]
        }
        
        return json
    }
}


//MARK: CNRegisterViewController Class
class CNRegisterViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var cellRowHeight: CGFloat = 80
    let checkboxIconString = ""
    let authenticationVM = AuthenticationViewModel()
    let getR2 = R2ViewModel()
    var r1 : String = ""
    var email : String = ""
    var cardNo : String = ""
    var formatType: String = ""
    var formJSON : JSON = JSON.null
    
    class func newInstance() -> UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "Register", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNRegisterViewController")
        
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Button Action
    @objc func selectCheckBoxDidTap(sender: CustomButton){
        let index = sender.tag
        let buttonNo = sender.buttonNo
        let paramKey = self.formJSON[index]["paramKey"].stringValue
        
        switch paramKey {
        case "gender":
            //First Button
            if buttonNo == 0 {
                self.formJSON[index]["isMale"] = JSON(true)
            }else{
                self.formJSON[index]["isMale"] = JSON(false)
            }
            self.tableView.reloadData()
            break 
        default:
            break
        }
    }
    func getR2s() {
        r1 = CNUtility.generateR1()
        let parameter : [String: Any] = ["r1": r1]
        self.getR2.getR2(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Register JSON Response: \(json)")
                    self.register(r2: json["result"]["r2"].stringValue)
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    func register(r2 : String) {
        
        let r3 = CNUtility.generateShar256(r1+r2)
        
        print("r1: \(r1)")
        print("r2: \(r2)")
        print("r3: \(r3)")
        let iv = CNUtility.generateShar256(UUID().uuidString)
        print("iv: \(iv)")
        //let encryptText = CNUtility.aesEncryptionWithCBC(text: "อยากได้โปรแกรมเมอร์นมใหญ่ๆ จัดให้หน่อยครับพี่แม็ก", key: r3, iv: iv)
        let encryptText = CNUtility.aesEncryptionWithECB(text: "Tae", key: r3)
        print("Encrypt: \(encryptText)")
        print("Decrypt: \(CNUtility.aesDecryptionWithECB(text: encryptText, key: r3))")
        //print(CNUtility.aesDecryptionWithCBC(text: encryptText, key: r3, iv: iv))
        
//        var parameter: [String: Any] = [:]
//
//        for i in 0..<self.formJSON.count-1 {
//            if  self.formJSON[i]["paramKey"].stringValue == "gender" {
//                var code = 0
//                if self.formJSON[i]["isMale"].boolValue {
//                    code = 0
//                }else{
//                    code = 1
//                }
//                parameter.updateValue("\(code)", forKey: self.formJSON[i]["paramKey"].stringValue)
//            }else{
//                if self.formJSON[i]["paramKey"].stringValue == "email" {
//                    email = self.formJSON[i]["value"].stringValue
//                }
//                if self.formJSON[i]["paramKey"].stringValue == "cardNo" {
//                    cardNo = self.formJSON[i]["value"].stringValue
//                }
//
//                parameter.updateValue(self.formJSON[i]["value"].stringValue, forKey: self.formJSON[i]["paramKey"].stringValue)
//            }
//
//        }
//        parameter.updateValue(self.r1, forKey: "r1")
//        print(parameter)
//        self.authenticationVM.registerUser(view:
//            self.navigationController?.view
//        , parameter: parameter) {
//            (status, json, error) in
//            switch status {
//            case .success:
//                if let json = json {
//                    print("Register JSON Response: \(json)")
//                    switch json["responseStatus"]["code"].intValue {
//                    case 00001 :
//                        self.navigateToEnterOTP(json: json)
//                    default : let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
//
//                        }))
//                        self.present(alert, animated: true, completion: nil)
//                    }
//
//                }
//                break
//            case .error:
//                break
//            case .failure:
//                break
//            case .unknown:
//                break
//            }
//        }
    }
    @objc func registerButtonDidTap(sender: UIButton){
        print("Check: formJSON \(self.formJSON.count)")
        getR2s()
       
    }

    
    // MARK: - Navigation
    func navigateToEnterOTP(json : JSON){
        if let vc = CNEnterOTPViewController.newInstance() as? CNEnterOTPViewController {
            vc.refCode = json["result"]["refCode"].stringValue
            vc.email = email
            vc.cardNo = cardNo
            vc.functionName = "register"
            vc.r1 = r1
            self.navigationController?.pushViewController(vc, animated: true)
            let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}

//MARK: - UITableView Delegate and DataSource
extension CNRegisterViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.formJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let formData = self.formJSON[indexPath.row]
        let cellType = formData["cellType"].stringValue
        switch cellType {
        case FormType.typeOne.rawValue:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CNFormTextFieldTypeOneTableViewCell", for: indexPath) as! CNFormTextFieldTypeOneTableViewCell
            cell.titleLabel.text = formData["title"].stringValue
            cell.textField.text = formData["value"].stringValue
            cell.textField.tag = indexPath.row
            cell.textField.delegate = self
            cell.textField.isSecureTextEntry = formData["isSecureText"].boolValue
            //cell.textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
            
            return cell
        case FormType.typeTwo.rawValue:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CNFormTextFieldTypeTwoTableViewCell", for: indexPath) as! CNFormTextFieldTypeTwoTableViewCell
            
            cell.titleLabel.text = formData["title"].stringValue
            cell.firstCheckboxLabel.text = formData["values"][0].stringValue
            cell.firstCheckboxView.layer.cornerRadius = cell.firstCheckboxView.bounds.width / 2
            cell.firstCheckboxView.layer.borderColor = UIColor.black.cgColor
            cell.firstCheckboxView.layer.borderWidth = 1
            cell.firstCheckboxButton.tag = indexPath.row
            cell.firstCheckboxButton.buttonNo = 0
            cell.firstCheckboxButton.addTarget(self, action: #selector(self.selectCheckBoxDidTap(sender:)), for: .touchUpInside)
            
            cell.secondCheckboxLabel.text = formData["values"][1].stringValue
            cell.secondCheckboxView.layer.cornerRadius = cell.secondCheckboxView.bounds.width / 2
            cell.secondCheckboxView.layer.borderColor = UIColor.black.cgColor
            cell.secondCheckboxView.layer.borderWidth = 1
            cell.secondCheckboxButton.tag = indexPath.row
            cell.secondCheckboxButton.buttonNo = 1
            cell.secondCheckboxButton.addTarget(self, action: #selector(self.selectCheckBoxDidTap(sender:)), for: .touchUpInside)
            
            let isMale = formData["isMale"].boolValue
            if isMale {
                cell.firstCheckboxIconLabel.text = self.checkboxIconString
                cell.secondCheckboxIconLabel.text = ""
            }else{
                cell.secondCheckboxIconLabel.text = self.checkboxIconString
                cell.firstCheckboxIconLabel.text = ""
            }
            
            return cell
        case FormType.typeButton.rawValue:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CNFormTypeButtonTableViewCell", for: indexPath) as! CNFormTypeButtonTableViewCell
            cell.cellButton.setTitle(formData["title"].stringValue, for: .normal)
            
            //Register Did Tap Action ..c -
            switch self.formatType {
            case CNRegisterFormatType.General.rawValue.lowercased():
                cell.cellButton.addTarget(self, action: #selector(self.registerButtonDidTap(sender:)), for: .touchUpInside)
            default:
                break
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellRowHeight
    }
}

//MARK: UITextField Delegate
extension CNRegisterViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let index = textField.tag
        let cellType = self.formJSON[index]["cellType"].stringValue
        
        switch cellType {
        case FormType.typeOne.rawValue:
            if let text = textField.text {
                let maxLength = 99
                let currentString: NSString = text as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                if text.count < maxLength {
                    self.formJSON[index]["value"] = JSON(String(newString))
                }
                return newString.length <= maxLength
            }
            break
        default:
            break
        }
        return true
    }
    
//    @objc func textFieldDidChange(textField: UITextField) {
//
//        let index = textField.tag
//        let cellType = self.formJSON[index]["cellType"].stringValue
//
//        switch cellType {
//        case "typeOne":
//            if let text = textField.text {
//                self.formJSON[index]["value"] = JSON(text)
//            }
//            break
//        default:
//            break
//        }
//    }
}
