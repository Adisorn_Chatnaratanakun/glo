//
//  AuthenticationLogInViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 7/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON

class AuthenticationLogInViewController: UIViewController {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var usernamneTextField: UITextField!
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var logInWithAgentButton: UIButton!
    @IBOutlet weak var logInWithGeneralButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var logInWithPinButton: UIButton!
    @IBOutlet weak var facebookLogInButton: FBSDKLoginButton!
    @IBOutlet weak var googleLogInButton: UIButton!
    @IBOutlet weak var changeURLButton: UIButton!
    
    let authenticationVM = AuthenticationViewModel()
    var r1 : String = ""
    var logInJSON: JSON = JSON.null

    let getR2 = R2ViewModel()
    var currentAPIPath: String = ""
    var nameFB = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.usernamneTextField.text = ""
        self.passwordTextField.text = ""
        self.view.endEditing(true)
    }
    
    
    func setupUI() {
        self.currentAPIPath = Constant.baseAPIURL
        self.changeURLButton.setTitle("Change URL: \(self.currentAPIPath)", for: .normal)
        
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
        
        self.usernamneTextField.isSecureTextEntry = false
        self.passwordTextField.isSecureTextEntry = true
        self.logInWithAgentButton.isHidden = false
        
        self.usernamneTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        
        //Button Action
        self.changeURLButton.addTarget(self, action: #selector(self.changeURLDidTap(sender:)), for: .touchUpInside)
        self.logInWithGeneralButton.addTarget(self, action: #selector(self.logInWithGeneralDidTap(sender:)), for: .touchUpInside)
        self.registerButton.addTarget(self, action: #selector(self.registerDidTap(sender:)), for: .touchUpInside)
        self.forgotPasswordButton.addTarget(self, action: #selector(self.forgotPasswordDidTap(sender:)), for: .touchUpInside)
        self.logInWithPinButton.addTarget(self, action: #selector(self.logInWithPinDidTap(sender:)), for: .touchUpInside)
        self.logInWithAgentButton.addTarget(self, action: #selector(self.logInWithAgentDidTap(sender:)), for: .touchUpInside)
//        self.facebookLogInButton.addTarget(self, action: #selector(self.facebookLogInDidTap(sender:)), for: .touchUpInside)
        self.googleLogInButton.addTarget(self, action: #selector(self.googleLogInDidTap(sender:)), for: .touchUpInside)
        
        facebookLogInButton.readPermissions = ["public_profile","email"]
        facebookLogInButton.delegate = self
    }
    
    //MARK: - Button Action
    @objc func logInWithGeneralDidTap(sender: UIButton) {
        self.getR2sForGeneral()
    }
    
    @objc func registerDidTap(sender: UIButton) {
        if let vc = CNRegisterViewController.newInstance() as? CNRegisterViewController {
            vc.formatType = CNRegisterFormatType.General.rawValue.lowercased()
            vc.formJSON = CNRegisterFormatType.General.formatData()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func forgotPasswordDidTap(sender: UIButton) {
        if let vc = CNForgotPasswordViewController.newInstance() as? CNForgotPasswordViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func logInWithPinDidTap(sender: UIButton) {
        self.navigateToLogInWithPINViewController()
    }
    
    @objc func logInWithAgentDidTap(sender: UIButton){
        self.getR2sForAgent()
    }
    
    @objc func facebookLogInDidTap(sender: UIButton) {
        
    }
    
    @objc func googleLogInDidTap(sender: UIButton) {
        
    }
    
    @objc func changeURLDidTap(sender: UIButton) {
        self.displayChangeURLAlert()
    }

    //MARK: Load Service
    func getR2sForGeneral() {
        self.r1 = CNUtility.generateR1()
        let parameter : [String: Any] = ["r1": r1]
        self.getR2.getR2(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Get R2 JSON Response: \(json)")
                    self.callGeneralLogInService(view: self.navigationController?.view, r2: json["result"]["r2"].stringValue)
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    func getR2sForAgent(){
        self.r1 = CNUtility.generateR1()
        let parameter : [String: Any] = ["r1": r1]
        self.getR2.getR2(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Get R2 JSON Response: \(json)")
                    self.callAgentLogInService(view: self.navigationController?.view, r2: json["result"]["r2"].stringValue)
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    func callGeneralLogInService(view: UIView?, r2: String){
        guard let username = self.usernamneTextField.text else{
            return
        }
        
        guard let password = self.passwordTextField.text else {
            return
        }
        
        let r3 = CNUtility.generateShar256(r1+r2)
        
        var parameter: [String: Any] = [
            "username": username,
            "password": password,
            "appVersion": AppInfo.APP_VERSION,
            "model": Device.DEVICE_MODEL+" "+Device.DEVICE_MODEL_NAME,
            "os": Singleton.shared.deviceType+" "+Device.DEVICE_SYSTEM_VERSION,
            "deviceId": Singleton.shared.getDeviceId(),
            "r1": self.r1
        ]
        
        parameter.updateValue(self.r1, forKey: "r1")
        print(parameter)
        
        self.authenticationVM.generalLogIn(view: view, parameter: parameter) {
            (status, json, error) in
            switch status{
            case .success:
                if let json = json {
                    self.logInJSON = json["result"]
                    let responseStatus = json["responseStatus"]
                    let message = responseStatus["message"].stringValue
                    let code = responseStatus["code"].stringValue
                    switch code {
                    //case
                    case "00000":
                        let authToken = self.logInJSON["authToken"].stringValue
                        Singleton.shared.saveAuthToken(token: authToken)
                        
                        let gloId = self.logInJSON["gloId"].stringValue
                        Singleton.shared.saveGloID(gloId: gloId)
                        
                        Singleton.shared.saveUserAccount(username: username, password: password)
                        self.navigateToHomeViewController()
                    case "30005": //FORCE_SET_PIN
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            Singleton.shared.saveUserAccount(username: username, password: password)
                            self.navigateToSetPinViewController()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    default:
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                    print(json)
                }
                break
            case .failure:
                print("fail")
                break
            case .error:
                print("error")
                break
            case .unknown:
                print("unknown")
                break
            }
        }
    }
    
    func callAgentLogInService(view: UIView?, r2: String){
        guard let username = self.usernamneTextField.text else{
            return
        }
        
        guard let password = self.passwordTextField.text else {
            return
        }
        
        let r3 = CNUtility.generateShar256(r1+r2)
        
        var parameter: [String: Any] = [
            "username": username,
            "password": password,
            "appVersion": AppInfo.APP_VERSION,
            "model": Device.DEVICE_MODEL+" "+Device.DEVICE_MODEL_NAME,
            "os": Singleton.shared.deviceType+" "+Device.DEVICE_SYSTEM_VERSION,
            "deviceId": Singleton.shared.getDeviceId(),
            "r1": self.r1
        ]
        
        parameter.updateValue(self.r1, forKey: "r1")
        print(parameter)
        
        self.authenticationVM.agentLogIn(view: view, parameter: parameter) {
            (status, json, error) in
            switch status{
            case .success:
                if let json = json {
                    self.logInJSON = json["result"]
                    let responseStatus = json["responseStatus"]
                    let message = responseStatus["message"].stringValue
                    let code = responseStatus["code"].stringValue
                    switch code {
                    //case
                    case "00000":
                        let authToken = self.logInJSON["authToken"].stringValue
                        Singleton.shared.saveAuthToken(token: authToken)
                        
                        let gloId = self.logInJSON["gloId"].stringValue
                        Singleton.shared.saveGloID(gloId: gloId)
                        
                        Singleton.shared.saveUserAccount(username: username, password: password)
                        self.navigateToHomeViewController()
                    case "30004"://FORCE_CHANGE_USERNAME
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            self.navigateToRegisterAgentViewController()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    case "30005": //FORCE_SET_PIN
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            Singleton.shared.saveUserAccount(username: username, password: password)
                            self.navigateToSetPinViewController()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    default:
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                    print(json)
                }
                break
            case .failure:
                print("fail")
                break
            case .error:
                print("error")
                break
            case .unknown:
                print("unknown")
                break
            }
        }
    }
    
    //MARK: Button Action
    @objc func displayChangeURLAlert(){
        let alertVC = UIAlertController.init(title: "Select API", message: nil, preferredStyle: .actionSheet)
        
        let devAction = UIAlertAction.init(title: "DEV", style: .default) { (action) in
            Singleton.shared.currentAPI = "dev"
            self.setBaseAPIURL()
            self.currentAPIPath = Constant.baseAPIURL
            DispatchQueue.main.async {
                self.changeURLButton.setTitle("Change URL: \(self.currentAPIPath)", for: .normal)
            }
        }
        
        let uatAction = UIAlertAction.init(title: "UAT", style: .default) { (action) in
            Singleton.shared.currentAPI = "uat"
            self.setBaseAPIURL()
            self.currentAPIPath = Constant.baseAPIURL
            DispatchQueue.main.async {
                self.changeURLButton.setTitle("Change URL: \(self.currentAPIPath)", for: .normal)
            }
        }
        
        let productionAction = UIAlertAction.init(title: "PRODUCTION", style: .default) { (action) in
            Singleton.shared.currentAPI = "prod"
            self.setBaseAPIURL()
            self.currentAPIPath = Constant.baseAPIURL
            DispatchQueue.main.async {
                self.changeURLButton.setTitle("Change URL: \(self.currentAPIPath)", for: .normal)
            }
        }
        
        alertVC.addAction(devAction)
        alertVC.addAction(uatAction)
        alertVC.addAction(productionAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func setBaseAPIURL(){
        switch  Singleton.shared.currentAPI{
        case "dev":
            Constant.baseAPIURL = Constant.devURL
        case "uat":
            Constant.baseAPIURL = Constant.uatURL
        case "prod":
            Constant.baseAPIURL = Constant.prodURL
        default:
            break
        }
    }
    
    // MARK: - Navigation
    func navigateToHomeViewController(){
        if let vc = CNHomeViewController.newInstance() as? CNHomeViewController {
            if nameFB != "" {
                vc.fromFB = true
                vc.fbname = nameFB
            }
            let nvc = UINavigationController.init(rootViewController: vc)
            self.present(nvc, animated: true, completion: nil)
        }
    }
    
    func navigateToSetPinViewController(){
        if let vc = CNSetPinViewController.newInstance() as? CNSetPinViewController {
            guard let username = self.usernamneTextField.text else{
                return
            }
            
            guard let password = self.passwordTextField.text else {
                return
            }
            vc.username = username
            vc.password = password
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateToLogInWithPINViewController(){
        if let vc = CNLogInWithPINViewController.newInstance() as? CNLogInWithPINViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateToRegisterAgentViewController(){
        if let vc = CNRegisterViewController.newInstance() as? CNRegisterViewController {
            vc.formatType = CNRegisterFormatType.Agent.rawValue.lowercased()
            vc.formJSON = CNRegisterFormatType.Agent.formatData()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}

//MARK: - UITextField Delegate
extension AuthenticationLogInViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(textField: UITextField) {
        switch textField {
        case self.usernamneTextField:
            if let text = textField.text {
                print("Username: \(text)")
            }
            break
        case self.passwordTextField:
            if let text = textField.text {
                print("Password: \(text)")
            }
            break
        default:
            break
        }
    }
}
extension AuthenticationLogInViewController : FBSDKLoginButtonDelegate  {
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        print(error , FBSDKSettings.sdkVersion() )
        if error == nil {
            print(result)
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name , first_name, last_name, picture.width(9999), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let data = JSON(result!)
                    print(JSON(result!))
                    self.nameFB = data["first_name"].stringValue+" "+data["last_name"].stringValue
                    self.navigateToHomeViewController()
//                    CNFunctionManage().analyticsFunctionEvent("Register", event: AnalyticsName.registerFace, label: "Register")
//                    self.connection.connectServiceWithProgressPOST(MethodName.prefixAuth+MethodName.authFacebook, view: self.view, params: ["first_name":data["first_name"].stringValue,"last_name":data["last_name"].stringValue,"email":data["email"].stringValue,"facebook_id":data["id"].stringValue,"facebook_image":data["picture"]["data"]["url"].stringValue,"version":"1.0.1"])
//                    Singleton.sharedInstance.name = data["id"].stringValue
//                    Singleton.sharedInstance.email = data["email"].stringValue
//                    UserDefaults.standard.set(Singleton.sharedInstance.name, forKey: "user")
//                    UserDefaults.standard.set(Singleton.sharedInstance.email, forKey: "email")
                }else{
                    print(error)
                }
            })
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }

    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
}

