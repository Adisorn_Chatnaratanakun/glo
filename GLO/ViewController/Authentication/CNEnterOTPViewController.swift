//
//  CNEnterOTPViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class CNEnterOTPViewController: UIViewController {
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var otpTitleLabel: UILabel!
    @IBOutlet weak var requestOTPButton: UIButton!
    @IBOutlet weak var sendOTPButton: UIButton!
    
    @IBOutlet weak var refCodeLabel: UILabel!
    var refCode : String = ""
    var cardNo : String = ""
    var r1 : String = ""
    var email : String = ""
    let verifyOTP = VerifyOTPViewModel()
    var functionName: String = ""
    class func newInstance() -> UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNEnterOTPViewController")
        
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    //MARK: - Set up
    func setupUI() {
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
        
        self.otpTextField.delegate = self
        
        //Button Action
        self.requestOTPButton.addTarget(self, action: #selector(self.requestOTPDidTap(sender:)), for: .touchUpInside)
        self.sendOTPButton.addTarget(self, action: #selector(self.sendOTPDidTap(sender:)), for: .touchUpInside)
        
        refCodeLabel.text = "Ref code : "+refCode
    }
    
    
    //MARK: Button Action
    @objc func requestOTPDidTap(sender: UIButton){
        print("Request OTP")
        let parameter : [String: Any] = ["gloId":"412025393341169665","email":email,"function":self.functionName,"r1": r1]
        self.verifyOTP.requestOTP(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Register JSON Response: \(json)")
                    switch json["responseStatus"]["code"].intValue {
                    case 00003 :
                        self.navigationController?.popToRootViewController(animated: false)
                        let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                        }))
                    default : let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    @objc func sendOTPDidTap(sender: UIButton){
        let parameter : [String: Any] = ["otp":otpTextField.text ?? "","cardNo":cardNo,"refCode":refCode,"r1": r1,"function":self.functionName]
        self.verifyOTP.verifyOTP(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Register JSON Response: \(json)")
                    switch json["responseStatus"]["code"].intValue {
                    case 00002 :
                        let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                            switch self.functionName {
                            case "updateProfile":
                                self.navigationController?.popViewController(animated: true)
                                break
                            default:
                                self.navigationController?.popToRootViewController(animated: true)
                                break
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case 00003 :
                        
                        let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                           
                            switch self.functionName {
                            case "forgotPassword":
                                self.navigateToSetNewPassword()
                                break
                            case "forgotPIN":
                                self.navigateToSetNewPIN()
                                break
                            default:
                                self.navigationController?.popToRootViewController(animated: true)
                                break
                            }
                        }))
                         self.present(alert, animated: true, completion: nil)
                    default : let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
        print("Send OTP: \(String(describing: self.otpTextField.text))")
    }
    

    
    // MARK: - Navigation
    func navigateToSetNewPassword(){
        if let vc = CNSetNewPasswordViewController.newInstance() as? CNSetNewPasswordViewController {
            vc.cardNo = self.cardNo
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateToSetNewPIN(){
        if let vc = CNSetNewPinViewController.newInstance() as? CNSetNewPinViewController {
            vc.cardNo = self.cardNo
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}

//MARK: UITextField Delegate
extension CNEnterOTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.otpTextField:
            if let text = textField.text {
                let maxLength = 6
                let currentString: NSString = text as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            break
        default:
            break
        }
        return true
    }
}
