//
//  CNSetPinViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 9/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import SwiftyJSON

class CNSetPinViewController: UIViewController {
    
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var pinTitleLabel: UILabel!
    
    var username: String = ""
    var password: String = ""
    var pin: String = ""
    var r1: String = ""
    let authenticationVM = AuthenticationViewModel()
    var setPinJSON: JSON = JSON.null
    let getR2 = R2ViewModel()
    
    class func newInstance() -> UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNSetPinViewController")
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    //MARK: - Set up
    func setupUI() {
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
        
        self.pinTextField.delegate = self
        
    }
    
    //MARK: - Load Service
    
    func getR2s() {
        r1 = CNUtility.generateR1()
        let parameter : [String: Any] = ["r1": r1]
        self.getR2.getR2(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Get R2 JSON Response: \(json)")
                    self.callSetPinService(r2: json["result"]["r2"].stringValue)
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    func callSetPinService(r2: String){
        
        let r3 = CNUtility.generateShar256(r1+r2)
        
        guard self.username != "" else{
            return
        }
        
        guard self.password != "" else {
            return
        }
        
        guard let pinText = self.pinTextField.text else{
            return
        }
        
        
        var parameter: [String: Any] = [
            "username": username,
            "password": password,
            "pin": pinText,
            "appVersion": AppInfo.APP_VERSION,
            "model": Device.DEVICE_MODEL,
            "os": Singleton.shared.deviceType+" "+Device.DEVICE_SYSTEM_VERSION,
            "deviceId": Singleton.shared.getDeviceId(),
            "r1": self.r1
        ]
        parameter.updateValue(self.r1, forKey: "r1")
        print("Set Pin Parameter: \(parameter)")
        self.authenticationVM.setPin(view: view, parameter: parameter) {
            (status, json, error) in
            switch status{
            case .success:
                if let json = json {
                    self.setPinJSON = json["result"]
                    let responseStatus = json["responseStatus"]
                    let message = responseStatus["message"].stringValue
                    let code = responseStatus["code"].intValue
                    switch code {
                    case 00000:
                        if let authToken = self.setPinJSON["authToken"].string {
                            Singleton.shared.saveAuthToken(token: authToken)
                            self.navigateToHomeViewController()
                        }
                        break
                    default:
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                    print(json)
                }
                break
            case .failure:
                print("fail")
                break
            case .error:
                print("error")
                break
            case .unknown:
                print("unknown")
                break
            }
        }
    }
    
    
    //MARK: Button Action

    
    
    
     // MARK: - Navigation
    func navigateToHomeViewController(){
        if let vc = CNHomeViewController.newInstance() as? CNHomeViewController {
            let nvc = UINavigationController.init(rootViewController: vc)
            self.present(nvc, animated: true, completion: nil)
        }
    }
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
    
    
}

//MARK: UITextField Delegate
extension CNSetPinViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.pinTextField:
            if let text = textField.text {
                let maxLength = 6
                let currentString: NSString = text as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                if newString.length == maxLength {
                    self.pinTextField.text = newString as String
                    self.getR2s()
                    return false
                }
                return newString.length <= maxLength
            }
            break
        default:
            break
        }
        return true
    }
}
