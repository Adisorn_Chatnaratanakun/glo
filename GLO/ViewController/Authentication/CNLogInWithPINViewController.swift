//
//  CNLogInWithPINViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 9/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import SwiftyJSON

class CNLogInWithPINViewController: UIViewController {
    
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var pinTitleLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPinButton: UIButton!
    
    let authenticationVM = AuthenticationViewModel()
    var logInWithPinJSON: JSON = JSON.null
    var r1: String = ""
    let getR2 = R2ViewModel()
    
    class func newInstance() -> UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNLogInWithPINViewController")
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    //MARK: - Set up
    func setupUI() {
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
        
        self.pinTextField.delegate = self
        
        //Button Action
        self.loginButton.addTarget(self, action: #selector(self.loginButtonDidTap(sender:)), for: .touchUpInside)
        self.forgotPinButton.addTarget(self, action: #selector(self.navigateToForgotPinViewController), for: .touchUpInside)
    }
    
    
    //MARK: Button Action
    @objc func requestOTPDidTap(sender: UIButton){
        print("Request OTP")
    }
    
    @objc func loginButtonDidTap(sender: UIButton){
        self.getR2s()
    }
    
    //MARK: Load Service
    func getR2s() {
        r1 = CNUtility.generateR1()
        let parameter : [String: Any] = ["r1": r1]
        self.getR2.getR2(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Get R2 JSON Response: \(json)")
                    self.logInWithPin(view: self.navigationController?.view, r2: json["result"]["r2"].stringValue)
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    func logInWithPin(view: UIView?, r2: String){
        let r3 = CNUtility.generateShar256(r1+r2)
        
        guard let pinText = self.pinTextField.text else {
            return
        }
        
        let username = Singleton.shared.getUserAccount().username()
        
        var parameter: [String: Any] = [
            "username": username,
            "pin": pinText,
            "appVersion": AppInfo.APP_VERSION,
            "model": Device.DEVICE_MODEL+" "+Device.DEVICE_MODEL_NAME ,
            "os": Singleton.shared.deviceType+" "+Device.DEVICE_SYSTEM_VERSION,
            "deviceId": Singleton.shared.getDeviceId(),
            "r1": self.r1
        ]
        
        parameter.updateValue(self.r1, forKey: "r1")
        print("Parameter: \(parameter)")
        self.authenticationVM.logInWithPin(view: view, parameter: parameter) {
            (status, json, error) in
            switch status{
            case .success:
                if let json = json {
                    self.logInWithPinJSON = json["result"]
                    let responseStatus = json["responseStatus"]
                    let message = responseStatus["message"].stringValue
                    let code = responseStatus["code"].stringValue
                    switch code {
                    //case
                    case "00000":
                        let authToken = self.logInWithPinJSON["authToken"].stringValue
                        Singleton.shared.saveAuthToken(token: authToken)
                        print("Check authToken: \(authToken)")
                        let gloId = self.logInWithPinJSON["gloId"].stringValue
                        Singleton.shared.saveGloID(gloId: gloId)
                    
                        self.navigateToHomeViewController()
                    default:
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                    print(json)
                }
                break
            case .failure:
                print("fail")
                break
            case .error:
                print("error")
                break
            case .unknown:
                print("unknown")
                break
            }
        }
    }
    
    
     // MARK: - Navigation
    func navigateToHomeViewController(){
        if let vc = CNHomeViewController.newInstance() as? CNHomeViewController {
            let nvc = UINavigationController.init(rootViewController: vc)
            self.present(nvc, animated: true, completion: nil)
        }
    }
    
    @objc func navigateToForgotPinViewController(){
        if let vc = CNForgotPinViewController.newInstance() as? CNForgotPinViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
    
    
}

//MARK: UITextField Delegate
extension CNLogInWithPINViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.pinTextField:
            if let text = textField.text {
                let maxLength = 6
                let currentString: NSString = text as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            break
        default:
            break
        }
        return true
    }
}
