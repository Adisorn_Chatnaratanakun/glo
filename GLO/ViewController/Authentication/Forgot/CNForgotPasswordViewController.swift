//
//  CNForgotPasswordViewController.swift
//  GLO
//
//  Created by Adisorn Chatnaratanakun on 8/1/2562 BE.
//  Copyright © 2562 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import SwiftyJSON

class CNForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var cellRowHeight: CGFloat = 80
    let checkboxIconString = ""
    let authenticationVM = AuthenticationViewModel()
    var r1: String = ""
    let getR2 = R2ViewModel()
    var email: String = ""
    var cardNo: String = ""
    let forgotVM = ForgotViewModel()
    
    var formJSON : JSON = [
        [
            "title": "Card ID"
            ,"value": ""
            ,"isTextFieldEnable": true
            ,"isSecureText": false
            ,"isEncrypt": true
            ,"keyboardType": "numpadType"
            ,"cellType": FormType.typeOne.rawValue
            ,"paramKey": "cardNo"
        ],
        [
            "title": "Email"
            ,"value": ""
            ,"isTextFieldEnable": true
            ,"isEncrypt": true
            ,"keyboardType": "default"
            ,"cellType": FormType.typeOne.rawValue
            ,"paramKey": "email"
        ],
        [
            "title": "Mobile"
            ,"value": ""
            ,"isTextFieldEnable": true
            ,"isEncrypt": true
            ,"keyboardType": "default"
            ,"cellType": FormType.typeOne.rawValue
            ,"paramKey": "mobile"
        ],
        [
            "title": "Forgot Password"
            ,"cellType": FormType.typeButton.rawValue
            ,"paramKey": "forgotPassword"
        ]
    ]
    
    class func newInstance() -> UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: "Forgot", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNForgotPasswordViewController")
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    //MARK: - Load Service
    func getR2s() {
        r1 = CNUtility.generateR1()
        let parameter : [String: Any] = ["r1": r1]
        self.getR2.getR2(view: self.navigationController?.view
        , parameter: parameter) { (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    print("Get R2 JSON Response: \(json)")
                    self.forgotGeneralPassword(view: self.navigationController?.view, r2: json["result"]["r2"].stringValue)
                }
                break
            case .error:
                break
            case .failure:
                break
            case .unknown:
                break
            }
        }
    }
    
    func forgotGeneralPassword(view: UIView?, r2: String){
        let r3 = CNUtility.generateShar256(r1+r2)
        
        var parameter: [String: Any] = [:]
        
        for i in 0..<self.formJSON.count-1 {
            if  self.formJSON[i]["paramKey"].stringValue == "gender" {
                var code = 0
                if self.formJSON[i]["isMale"].boolValue {
                    code = 0
                }else{
                    code = 1
                }
                parameter.updateValue("\(code)", forKey: self.formJSON[i]["paramKey"].stringValue)
            }else{
                if self.formJSON[i]["paramKey"].stringValue == "email" {
                    email = self.formJSON[i]["value"].stringValue
                }
                if self.formJSON[i]["paramKey"].stringValue == "cardNo" {
                    cardNo = self.formJSON[i]["value"].stringValue
                }
                
                parameter.updateValue(self.formJSON[i]["value"].stringValue, forKey: self.formJSON[i]["paramKey"].stringValue)
            }
            
        }
        let model = Device.DEVICE_MODEL+" "+Device.DEVICE_MODEL_NAME
        parameter.updateValue(model, forKey: "model")
        parameter.updateValue(self.r1, forKey: "r1")
        print(parameter)
        
        self.forgotVM.forgotGeneralPassword(view: view, parameter: parameter) {
            (status, json, error) in
            switch status {
            case .success:
                if let json = json {
                    let responseStatus = json["responseStatus"]
                    let message = responseStatus["message"].stringValue
                    let code = responseStatus["code"].stringValue
                    switch code {
                    case "00001":
                        self.navigateToEnterOTP(json: json)
                    default:
                        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                }
                break
            case .failure:
                break
            case .error:
                break
            case .unknown:
                break
            }
        }
    }
    
    
    // MARK: - Navigation
    func navigateToEnterOTP(json : JSON){
        if let vc = CNEnterOTPViewController.newInstance() as? CNEnterOTPViewController {
            vc.refCode = json["result"]["refCode"].stringValue
            vc.email = email
            vc.cardNo = cardNo
            vc.functionName = "forgotPassword"
            vc.r1 = r1
            self.navigationController?.pushViewController(vc, animated: true)
            let alert = UIAlertController(title: "Message", message: json["responseStatus"]["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (alerts) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Button Action
    @objc func selectCheckBoxDidTap(sender: CustomButton){
        let index = sender.tag
        let buttonNo = sender.buttonNo
        let paramKey = self.formJSON[index]["paramKey"].stringValue
        
        switch paramKey {
        case "gender":
            //First Button
            if buttonNo == 0 {
                self.formJSON[index]["isMale"] = JSON(true)
            }else{
                self.formJSON[index]["isMale"] = JSON(false)
            }
            self.tableView.reloadData()
            break
        default:
            break
        }
    }
    
    @objc func forgotPasswordButtonDidTap(sender: UIButton){
        print("Check: formJSON \(self.formJSON)")
        self.getR2s()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: - UITableView Delegate and DataSource
extension CNForgotPasswordViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.formJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let formData = self.formJSON[indexPath.row]
        let cellType = formData["cellType"].stringValue
        switch cellType {
        case FormType.typeOne.rawValue:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CNFormTextFieldTypeOneTableViewCell", for: indexPath) as! CNFormTextFieldTypeOneTableViewCell
            cell.titleLabel.text = formData["title"].stringValue
            cell.textField.text = formData["value"].stringValue
            cell.textField.tag = indexPath.row
            cell.textField.delegate = self
            cell.textField.isSecureTextEntry = formData["isSecureText"].boolValue
            //cell.textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
            
            return cell
        case FormType.typeTwo.rawValue:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CNFormTextFieldTypeTwoTableViewCell", for: indexPath) as! CNFormTextFieldTypeTwoTableViewCell
            
            cell.titleLabel.text = formData["title"].stringValue
            cell.firstCheckboxLabel.text = formData["values"][0].stringValue
            cell.firstCheckboxView.layer.cornerRadius = cell.firstCheckboxView.bounds.width / 2
            cell.firstCheckboxView.layer.borderColor = UIColor.black.cgColor
            cell.firstCheckboxView.layer.borderWidth = 1
            cell.firstCheckboxButton.tag = indexPath.row
            cell.firstCheckboxButton.buttonNo = 0
            cell.firstCheckboxButton.addTarget(self, action: #selector(self.selectCheckBoxDidTap(sender:)), for: .touchUpInside)
            
            cell.secondCheckboxLabel.text = formData["values"][1].stringValue
            cell.secondCheckboxView.layer.cornerRadius = cell.secondCheckboxView.bounds.width / 2
            cell.secondCheckboxView.layer.borderColor = UIColor.black.cgColor
            cell.secondCheckboxView.layer.borderWidth = 1
            cell.secondCheckboxButton.tag = indexPath.row
            cell.secondCheckboxButton.buttonNo = 1
            cell.secondCheckboxButton.addTarget(self, action: #selector(self.selectCheckBoxDidTap(sender:)), for: .touchUpInside)
            
            let isMale = formData["isMale"].boolValue
            if isMale {
                cell.firstCheckboxIconLabel.text = self.checkboxIconString
                cell.secondCheckboxIconLabel.text = ""
            }else{
                cell.secondCheckboxIconLabel.text = self.checkboxIconString
                cell.firstCheckboxIconLabel.text = ""
            }
            
            return cell
        case FormType.typeButton.rawValue:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CNFormTypeButtonTableViewCell", for: indexPath) as! CNFormTypeButtonTableViewCell
            cell.cellButton.setTitle(formData["title"].stringValue, for: .normal)
            cell.cellButton.addTarget(self, action: #selector(self.forgotPasswordButtonDidTap(sender:)), for: .touchUpInside)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellRowHeight
    }
}

//MARK: UITextField Delegate
extension CNForgotPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let index = textField.tag
        let cellType = self.formJSON[index]["cellType"].stringValue
        
        switch cellType {
        case FormType.typeOne.rawValue:
            if let text = textField.text {
                let maxLength = 99
                let currentString: NSString = text as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                if text.count < maxLength {
                    self.formJSON[index]["value"] = JSON(String(newString))
                }
                return newString.length <= maxLength
            }
            break
        default:
            break
        }
        return true
    }
    
    //    @objc func textFieldDidChange(textField: UITextField) {
    //
    //        let index = textField.tag
    //        let cellType = self.formJSON[index]["cellType"].stringValue
    //
    //        switch cellType {
    //        case "typeOne":
    //            if let text = textField.text {
    //                self.formJSON[index]["value"] = JSON(text)
    //            }
    //            break
    //        default:
    //            break
    //        }
    //    }
}
